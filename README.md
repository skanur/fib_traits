# Description
Public repo for [this](http://codereview.stackexchange.com/questions/130042/implement-a-generic-fibonacci-sequence-in-rust-without-using-copy-trait?noredirect=1#comment243492_130042) question.

# Install
* Clone the project to your local directory and `cd` to it
* run `cargo build && cargo test`
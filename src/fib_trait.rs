extern crate num;

pub mod fib_trait {

    use fib_trait::num::traits::*;
    use std::ops::Add;

    #[derive(Clone, Copy)]
    pub struct Fibonacci<T> where
    T : Zero + One + Add + Copy {
        curr : T,
        next : T,
    }

    impl<T> Fibonacci<T> where 
    T : Zero + One + Add  + Copy{
        pub fn new() -> Fibonacci<T> {
            Fibonacci {
                curr : T::zero(),
                next : T::one(),
            }
        }
    }

    impl<T> Iterator for Fibonacci<T> where
    T : Zero + One + Add + Copy {
        type Item = T;

        fn next(&mut self) -> Option<T> {
            let c : T = self.next;
            self.next = self.next + self.curr;
            self.curr = c;
            let n : T = self.next;
            Some(n)
        }
    }

    #[test]
    pub fn fib_test() {
        let mut f : Fibonacci<u32> = Fibonacci::new();
        assert_eq!(f.next(), Some(1));

        let mut next_val = 1;
        let mut curr_val = 0; 
        for i in Fibonacci::<u32>::new().take(4) {
            let c = next_val;
            next_val = curr_val + next_val;
            curr_val = c;
            assert_eq!(i, next_val);
        }
    }
}
